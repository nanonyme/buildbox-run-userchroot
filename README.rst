What is buildbox-run-userchroot?
===============================

``buildbox-run-userchroot`` attempts to run actions within a chroot.
``buildbox-run-userchroot`` depends on userchroot being installed on the system.
Note that userchroot is an opensource tool: https://github.com/bloomberg/userchroot,
and is seperate from generic chroot-jails like the one provided by GNU coreutils.

Usage
=====
::

    usage: ./buildbox-run-userchroot [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-directory=DIR         Write logs to this directory with filenames:
                                    buildbox-run-userchroot.<hostname>.<user name>.log.<severity level>.<date>.<time>.<pid>
        --use-localcas              Use LocalCAS protocol methods (default behavior)
                                    NOTE: this option will be deprecated.
        --disable-localcas          Do not use LocalCAS protocol methods
        --workspace-path=PATH       Location on disk which runner will use as root when executing jobs
        --stdout-file=FILE          File to redirect the command's stdout to
        --stderr-file=FILE          File to redirect the command's stderr to
        --no-logs-capture           Do not capture and upload the contents written to stdout and stderr
        --capabilities              Print capabilities supported by this runner
        --validate-parameters       Only check whether all the required parameters are being passed and that no
                                    unknown options are given. Exits with a status code containing the result (0 if successful).
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --access-token=PATH         Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token.
        --token-reload-interval=MINUTES Time to wait before refreshing access token from disk again. The following suffixes can be optionally specified: M (minutes), H (hours). Value defaults to minutes if suffix not specified.
        --googleapi-auth            Use GoogleAPIAuth when this flag is set.
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=MILLISECONDS  How long to wait before the first grpc retry
        --userchroot-bin=PATH       Path to userchroot executable. Will default to looking in PATH if not set.

Building buildbox-run-userchroot
================================

Building ``buildbox-run-userchroot`` is easy, after building and installing
`buildbox-common`_. Change directories into this repo, and then run::

    mkdir build
    cd build
    cmake .. && make
    make test

.. _buildbox-common: https://gitlab.com/BuildGrid/buildbox/buildbox-common

Buildbox-run-userchroot prerequisites
=====================================

In order to use ``buildbox-run-userchroot``, `userchroot`_ must be built and installed.
You must then specify the path to ``userchroot`` with the ``--userchroot-bin`` flag. Note that
this flag can be specified in `buildbox-worker`_, as a ``--runner-arg`` flag.

It is also highly recommended that `buildbox-casd`_ is used along side ``buildbox-run-userchroot``.
This is because buildbox-run-userchroot will stage an entire chroot image on every execution
request. Fetching this chroot from ``CAS`` is likely to be expensive without local caching.

.. _userchroot: https://github.com/bloomberg/userchroot
.. _buildbox-worker: https://gitlab.com/BuildGrid/buildbox/buildbox-worker
.. _buildbox-casd: https://gitlab.com/BuildGrid/buildbox/buildbox-casd

Building and uploading chroot images
====================================

To use ``buildbox-run-userchroot``, a chroot image must first be built and uploaded
to ``CAS``. A chroot image is simply a filesystem image, within this context. There are
many ways to build or install one, such as `debootstrap`_. However, in this tutorial,
`chrootbuilder`_ will be used. Since it is more flexible and will ensure that the chroot
is compatible with buildbox-run-userchroot.

To use ``chrootbuilder``, simply specify a docker image or container to use. That image/container's
filesystem will exported as the chroot (Note that this process will not modify the docker image or
container). In this tutorial, the `gcc`_ docker image will be used::

    docker pull gcc@sha256:1cd1f0b466eab282b6c053e739e8510e4364fa9e91f337ba15d4a73978c3674f
    chrootbuilder -e gcc@sha256:1cd1f0b466eab282b6c053e739e8510e4364fa9e91f337ba15d4a73978c3674f chroot

Once this is done, the chroot image must be uploaded to ``CAS``. This can be done using the
`casupload`_ tool, which is included in `recc`_. This command will also calculate
the root digest::

    export RECC_CAS_SERVER=$YOUR_CAS_SERVER && export RECC_INSTANCE=$YOUR_INSTANCE
    casupload chroot

The root digest for this chroot image should be
``CHROOT_DIGEST=3e74aa72ba495b43abd18a32c4692e8ed73e808f75edcc18234b6dd66385d70d/1544``.
This digest should then be specified to ``buildbox-worker`` by passing it
``--platform chrootRootDigest=${CHROOT_DIGEST}``. It should also be specified for recc, using
``export RECC_REMOTE_PLATFORM_chrootRootDigest=${CHROOT_DIGEST}``. The ``chrootRootDigest`` key word
should also be added to the buildgrid config, as one of the ``property-keys`` under ``!execution``.
Check the `reference`_ for how to do this.

Once this setup is done, the chroot will be ready to use. ``buildbox-run-userchroot`` will use the
chroot as its sandboxing environment. Make sure to run it with ``buildbox-casd`` to have good performance.

One can find more explicit details on how to upload and use a chroot, along with ``buildbox-run-userchroot``
and ``buildbox-casd``, by checking ``test-recc-output.sh`` in the ``buildbox-e2e` repo. It uploads an alpine image,
and tests an execution request with ``buildbox-run-userchroot`` and ``buildbox-casd``.

.. _debootstrap: https://manpages.debian.org/stretch/debootstrap/debootstrap.8.en.html
.. _chrootbuilder: https://gitlab.com/BuildGrid/buildbox/buildbox-tools/-/tree/master/python/chrootbuilder
.. _gcc: https://github.com/docker-library/gcc
.. _casupload: https://gitlab.com/BuildGrid/buildbox/buildbox-tools#casupload
.. _recc: https://gitlab.com/BuildGrid/recc
.. _reference: https://gitlab.com/BuildGrid/buildgrid/-/blob/master/buildgrid/_app/settings/reference.yml

Avoiding the propagation of working directories with a buildbox-casd proxy
--------------------------------------------------------------------------
When connected to a casd instance, it will be used to stage the directory in
which the action will be executed. For that buildbox-run-userchroot will need
to add to CAS a directory structure that consists of the inputs of the action
merged with the chroot image.

Since that directory tree is exclusive to a particular action, it could be
desirable to not have it be propagated to a remote CAS server. That can be
achieved by configuring casd with a server instance name
(``--server-instance=NAME``) and passing ``--local-only-cas-instance-name=NAME``
to buildbox-run-userchroot.


Limitation
==========

Userchroot has some limitations, for example the execution request should not modify the root directory. This
is because ``userchroot`` requires the chroot image's root to  have strict permissions. `buildbox-casd`_ should
also run as a different user than ``buildbox-run-userchroot``. If not, one risks execution requests corrupting the
local cache.

.. _buildbox-casd: https://gitlab.com/BuildGrid/buildbox/buildbox-casd
