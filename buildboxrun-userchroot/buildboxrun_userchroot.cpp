/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_userchroot.h>

#include <buildboxrun_chrootmanager.h>

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_grpcclient.h>
#include <buildboxcommon_grpcerror.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_runner.h>
#include <buildboxcommon_scopeguard.h>

#include <array>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace userchroot {

namespace {

/**
 * Check Platform properties for the chroot root digest,
 * ensure that's not empty and populate output parameter 'chrootDigest'
 */
bool isMergeRequest(const Platform &platform, Digest *chrootDigest)
{
    static const char chrootPropName[] = "chrootRootDigest";
    for (const auto &property : platform.properties()) {
        if (property.name() == chrootPropName) {
            const std::string &hash = property.value();
            if (hash.empty()) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error, "Found invalid '"
                                            << chrootPropName
                                            << "' value with an empty hash");
            }

            const char *slash = strchr(hash.c_str(), '/');
            if (slash == nullptr) {
                BUILDBOXCOMMON_THROW_EXCEPTION(
                    std::runtime_error,
                    "Found invalid '"
                        << chrootPropName
                        << "' value with valid hash, but no blob size");
            }

            chrootDigest->set_hash(
                std::string(hash.c_str(),
                            static_cast<unsigned long>(slash - hash.c_str())));
            chrootDigest->set_size_bytes(std::stoll(slash + 1));
            return true;
        }
    }

    return false;
}

} // namespace

ActionResult UserChrootRunner::execute(const Command &command,
                                       const Digest &inputRootDigest,
                                       const Platform &platform)
{
    // Check the Platform properties to see if we need to merge
    Digest digest(inputRootDigest);
    Digest chrootDigest;
    if (isMergeRequest(platform, &chrootDigest)) {
        if (d_use_localcas_protocol) {
            // Making sure that the chroot is pre-loaded in local CAS if
            // connected to one.
            fetchChrootIntoLocalCas(chrootDigest);
        }

        Digest mergedDigest;
        mergeDigests(inputRootDigest, chrootDigest, &mergedDigest);
        BUILDBOX_RUNNER_LOG(DEBUG, "inputRootDigest = "
                                       << inputRootDigest
                                       << ", chrootDigest = " << chrootDigest
                                       << ", mergedDigest = " << mergedDigest);
        digest.Swap(&mergedDigest);
    }
    else {
        // The root directory must have `unix_mode = 0755` to signal casd that
        // it requires to be staged with those permissions.
        Directory rootDirectory =
            d_casClient->fetchMessage<Directory>(inputRootDigest);

        const auto rootDirectoryUnixMode =
            rootDirectory.node_properties().unix_mode().value();
        if (rootDirectoryUnixMode != s_rootDirectoryRequiredUnixMode) {
            BUILDBOX_RUNNER_LOG(
                DEBUG,
                "inputRootDigest="
                    << inputRootDigest
                    << " does not have node_properties.unix_mode == "
                    << s_rootDirectoryRequiredUnixMode
                    << ". Setting it and uploading the new Directory to CAS.");

            rootDirectory.mutable_node_properties()
                ->mutable_unix_mode()
                ->set_value(s_rootDirectoryRequiredUnixMode);

            const Digest newRootDirectoryDigest =
                d_casClient->uploadMessage(rootDirectory);

            BUILDBOX_RUNNER_LOG(DEBUG, "inputRootDigest="
                                           << inputRootDigest
                                           << " is now rooted at "
                                           << newRootDirectoryDigest << ".");

            digest = std::move(newRootDirectoryDigest);
        }
    }

    ActionResult result;
    auto *result_metadata = result.mutable_execution_metadata();

    Runner::metadata_mark_input_download_start(result_metadata);
    const auto stagedDir = this->stageDirectory(digest);
    Runner::metadata_mark_input_download_end(result_metadata);

    std::ostringstream workingDir;
    workingDir << stagedDir->getPath() << "/" << command.working_directory();

    // Verify that the required directories (such as "/dev") exist in the
    // staged directory.
    // If that is not the case, throw and cause the runner to return that error
    // in the `ActionResult`:
    assertRequiredDirectoriesExist();

    // Making sure that the permissions of the stage directory are set to
    // 0777 when this function is done so that it can later be cleaned up.
    const std::string stagedDirPath(stagedDir->getPath());
    const buildboxcommon::ScopeGuard chmodGuard([stagedDirPath]() {
        buildboxcommon::Runner::recursively_chmod_directories(
            stagedDirPath.c_str(), 0777);
    });

    {
        createOutputDirectories(command, workingDir.str());

        // userchroot doesn't allow symlinks in the root path
        char resolved_path[PATH_MAX];
        if (realpath(stagedDir->getPath(), resolved_path) == NULL) {
            BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
                std::system_error, errno, std::system_category,
                "error in realpath() failed for path \""
                    << stagedDir->getPath() << "\"");
        }

        ChrootManager userChrootMgr(d_userchroot_bin, resolved_path);
        // environment variables set in file, which is sourced in command
        const std::vector<std::string> commandLine =
            userChrootMgr.generateCommandLine(command);

        BUILDBOX_RUNNER_LOG(
            DEBUG, "Executing " << logging::printableCommandLine(commandLine));
        executeAndStore(commandLine, &result);
    }

    BUILDBOX_RUNNER_LOG(DEBUG, "Capturing command outputs...");
    Runner::metadata_mark_output_upload_start(result_metadata);
    stagedDir->captureAllOutputs(command, &result);
    Runner::metadata_mark_output_upload_end(result_metadata);

    BUILDBOX_RUNNER_LOG(DEBUG, "Finished capturing command outputs");
    return result;
}

void UserChrootRunner::printSpecialUsage()
{
    std::clog << "    --userchroot-bin=PATH       Path to userchroot "
                 "executable. Will default to looking in PATH if not set.\n";
    std::clog
        << "    --local-only-cas-instance-name  CAS instance name used "
           "to upload blobs of merged chroot trees.\n"
           "                                    This allows switching from "
           "a proxy to a server instance to prevent those blobs from\n"
           "                                    being forwarded to a remote "
           "CAS (they are only needed in the local CAS for staging).";
}

bool UserChrootRunner::parseArg(const char *arg)
{
    assert(arg);

    if (this->d_casRemote.parseArg(arg)) {
        // The optional local-only CAS uses the same connection options as the
        // regular CAS connection. `buildboxcommon::Runner` doesn't expose
        // these connection options, so parse them here as well but return
        // false such that `Runner` can still use them for the regular CAS
        // connection.
        return false;
    }

    const char *assign = strchr(arg, '=');

    bool argumentParsed = false;
    if (arg[0] == '-' && arg[1] == '-') {
        arg += 2;
        if (assign) {
            const auto key_len = static_cast<size_t>(assign - arg);
            const char *value = assign + 1;
            if (strncmp(arg, "userchroot-bin", key_len) == 0) {
                this->d_userchroot_bin = std::string(value);
                argumentParsed = true;
            }
            else if (strncmp(arg, "local-only-cas-instance-name", key_len) ==
                     0) {
                this->d_localOnlyCasInstanceName = std::string(value);
                argumentParsed = true;
            }
        }
    }

    return argumentParsed;
}

Digest UserChrootRunner::mergeTrees(const MergeUtil::DirectoryTree &inputTree,
                                    const MergeUtil::DirectoryTree &chrootTree,
                                    digest_string_map *mergedDirectoryBlobs,
                                    MergeUtil::DigestVector *newDigests)
{
    Digest mergedRootDigest;
    const bool success =
        MergeUtil::createMergedDigest(inputTree, chrootTree, &mergedRootDigest,
                                      mergedDirectoryBlobs, newDigests);
    if (success) {
        return mergedRootDigest;
    }
    else {
        return Digest();
    }
}

void UserChrootRunner::fetchChrootIntoLocalCas(const Digest &digest) const
{
    BUILDBOX_RUNNER_LOG(INFO, "Calling LocalCAS.FetchTree(chrootDigest="
                                  << digest
                                  << ") to make sure that the chroot is "
                                     "available from Local CAS");

    try {
        const bool fetchFiles = true;
        d_casClient->fetchTree(digest, fetchFiles);
    }
    catch (const buildboxcommon::GrpcError &e) {
        BUILDBOX_RUNNER_LOG(ERROR, "LocalCAS.FetchTree(chrootDigest="
                                       << digest
                                       << ") failed: " << e.status.error_code()
                                       << ": " << e.status.error_message());
        throw e;
    }
}

void UserChrootRunner::uploadMissingBlobs(const std::vector<Digest> &digests,
                                          const digest_string_map &data) const
{
    // A different instance name could have been specified for these requests.
    // This special case allows switching casd instances from a proxy to a
    // server so that the blobs in the merged tree do not end up being
    // forwarded to a remote CAS and only kept locally.
    auto casClient = d_casClient;
    if (!d_localOnlyCasInstanceName.empty()) {
        BUILDBOX_LOG_DEBUG("Uploading blobs to local-only CAS instance \""
                           << d_localOnlyCasInstanceName << "\"");

        // Use the same connection options as the regular CAS connection with
        // the exception of the instance name.
        ConnectionOptions localOnlyCasRemote = d_casRemote;
        localOnlyCasRemote.d_instanceName = d_localOnlyCasInstanceName;

        auto client = std::make_shared<GrpcClient>();
        client->init(localOnlyCasRemote);
        casClient = std::make_shared<CASClient>(std::move(client));
        casClient->init();
    }

    const std::vector<Digest> missingDigests =
        casClient->findMissingBlobs(digests);

    if (missingDigests.empty()) {
        return;
    }

    BUILDBOX_RUNNER_LOG(DEBUG, "Uploading " << missingDigests.size()
                                            << " missing blob(s) out of "
                                            << digests.size() << " total");

    std::vector<CASClient::UploadRequest> uploadRequests;
    uploadRequests.reserve(missingDigests.size());
    for (const auto &digest : missingDigests) {
        uploadRequests.emplace_back(digest, data.at(digest));
    }

    const std::vector<CASClient::UploadResult> results =
        casClient->uploadBlobs(uploadRequests);

    // verify that all blobs have been successfully uploaded
    // throw on any failures
    bool uploadSuccess = true;
    std::ostringstream oss;
    for (const auto &result : results) {
        if (!result.status.ok()) {
            oss << "Failed to upload a merged digest(" << result.digest
                << "), status = [" << result.status.error_code() << ": \""
                << result.status.error_message() << "\"]\n";
            uploadSuccess = false;
        }
    }

    if (!uploadSuccess) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, oss.str());
    }
}

Digest UserChrootRunner::mergeTreesAndSetRootDirectoryProperty(
    const MergeUtil::DirectoryTree &inputTree,
    const MergeUtil::DirectoryTree &chrootTree,
    digest_string_map *mergedDirectoryBlobs,
    MergeUtil::DigestVector *newDigests)
{
    digest_string_map blobs;
    MergeUtil::DigestVector newDigestList;

    // Merging the two trees:
    const Digest rootDigest =
        mergeTrees(inputTree, chrootTree, &blobs, &newDigestList);

    if (rootDigest == Digest()) {
        return Digest();
    }

    // The merge succeeded. We will now modify the root `Directory` of the
    // result to set its `unix_mode` property:
    Directory newRootDirectory;
    newRootDirectory.ParseFromString(blobs.at(rootDigest));
    newRootDirectory.mutable_node_properties()->mutable_unix_mode()->set_value(
        s_rootDirectoryRequiredUnixMode);

    // Serializing and recalculating its hash:
    const std::string newRootDirectoryBlob =
        newRootDirectory.SerializeAsString();
    const Digest newRootDirectoryDigest = CASHash::hash(newRootDirectoryBlob);

    // And swapping it for the old root:
    blobs.emplace(newRootDirectoryDigest, newRootDirectoryBlob);
    blobs.erase(rootDigest);

    auto it =
        std::find(newDigestList.begin(), newDigestList.end(), rootDigest);
    if (it != newDigestList.end()) {
        *it = newRootDirectoryDigest;
    }
    else {
        newDigestList.push_back(newRootDirectoryDigest);
    }

    // Everything succeeded, we can write to the output arguments:
    *mergedDirectoryBlobs = std::move(blobs);
    *newDigests = std::move(newDigestList);

    return newRootDirectoryDigest;
}

void UserChrootRunner::mergeDigests(const Digest &inputDigest,
                                    const Digest &chrootDigest,
                                    Digest *mergedRootDigest)
{
    // acquire the input trees of both digests
    // getTree will throw on error
    const MergeUtil::DirectoryTree inputTree =
        this->d_casClient->getTree(inputDigest);

    const MergeUtil::DirectoryTree chrootTree =
        this->d_casClient->getTree(chrootDigest);

    digest_string_map mergedDirectoryBlobs;
    MergeUtil::DigestVector newDigests;
    const Digest rootDigest = mergeTreesAndSetRootDirectoryProperty(
        inputTree, chrootTree, &mergedDirectoryBlobs, &newDigests);

    if (rootDigest == Digest()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Error merging inputDigest("
                                    << inputDigest << ") with chrootDigest("
                                    << chrootDigest << ")");
    }

    // Upload any missing digests to CAS so they can be staged
    uploadMissingBlobs(newDigests, mergedDirectoryBlobs);

    *mergedRootDigest = rootDigest;
}

void UserChrootRunner::assertRequiredDirectoriesExist() const
{
    static const std::array<std::string, 2> expected_directories = {"/dev",
                                                                    "/tmp"};

    for (const auto &directory : expected_directories) {
        const std::string expected_path = this->d_stage_path + directory;

        if (!FileUtils::isDirectory(expected_path.c_str())) {
            std::ostringstream errMsg;
            errMsg << "Staged chroot does not contain expected directory \""
                   << directory << "\"";

            this->writeErrorStatusFile(grpc::StatusCode::FAILED_PRECONDITION,
                                       errMsg.str());
            BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, errMsg.str());
        }
    }
}

} // namespace userchroot
} // namespace buildboxrun
} // namespace buildboxcommon
